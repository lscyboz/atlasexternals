# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Sets:
#  PYANALYSIS_BINARY_PATH
#  PYANALYSIS_PYTHON_PATH
#
# Can be steered by PYANALYSIS_ROOT.
#

# The LCG include(s):
include( LCGFunctions )

# If it was already found, let's be quiet:
if( PYANALYSIS_FOUND )
   set( pyanalysis_FIND_QUIETLY TRUE )
endif()

# Ignore system paths when an LCG release was set up:
if( PYANALYSIS_ROOT )
   set( _extraPyAnaArgs NO_SYSTEM_ENVIRONMENT_PATH NO_CMAKE_SYSTEM_PATH )
endif()

# Find the binary path:
find_path( PYANALYSIS_BINARY_PATH isympy
   PATH_SUFFIXES bin PATHS ${PYANALYSIS_ROOT}
   ${_extraPyAnaArgs} )

# Find the python path:
find_path( PYANALYSIS_PYTHON_PATH 
   NAMES site.py numpy
   PATH_SUFFIXES lib/python2.7/site-packages
   PATHS ${PYANALYSIS_ROOT}
   ${_extraPyAnaArgs} )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( pyanalysis DEFAULT_MSG
   PYANALYSIS_PYTHON_PATH PYANALYSIS_BINARY_PATH )

# Set up the RPM dependency:
lcg_need_rpm( pyanalysis )

# Clean up:
if( _extraPyAnaArgs )
   unset( _extraPyAnaArgs )
endif()
